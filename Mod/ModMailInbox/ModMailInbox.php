<?php

    include_once '../../utilities/checkRequestMethod.php';

    Class ModMailInbox {
        //DB Stuff
        private $conn;

        //Inbox Receiver Properties
        public $NId;
        public $StatusReceive;
        public $Nomor;
        public $InstansiPengirim;
        public $Hal;
        public $Tgl;
        public $ReceiveDate;
        public $zz;
        public $ds;


        // Constructor with DB
        public function __construct($db){
            $this->conn = $db;
        }

        public function test(){
            $query = "
            SELECT * FROM inbox_receiver rr 
		JOIN people p ON p.PeopleId = rr.To_Id AND p.PeopleIsActive = '1'
		WHERE (rr.RoleId_To LIKE 'uk.1.1.18%' ) AND rr.StatusReceive LIKE '%read%' 
		AND (rr.ReceiveDate BETWEEN '2021-01-01 00:00:00' AND '2021-08-30 23:59:59') 
		ORDER BY ReceiveDate DESC
            ";

            $stmt = $this->conn->prepare($query);
            
            //Execute query
            $stmt->execute();
                       
            // The Result send to API Endpoint on API folder
            return $stmt;

        }
        
        
        // Getting inbox_receiver from DB
        public function read($begin, $row, $primaryroleid, $sumber, $read, $tgl1,$tgl2,$unit,$sort,$search,$q){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            $page = ($begin*$row) - $row;
            // echo $begin; die();
            
            if($read == "read"){
                $read1 = 'AND (rr.StatusReceive="read")'  ;
                
            } elseif($read == "unread"){
                $read1 = 'AND (rr.StatusReceive="unread")'  ;
            } else {
                $read1 = ' ';
            }
            // echo $read1;die();
            if($search =="hal"){
                $search1 = "AND (Hal LIKE '%$q%' )";
            } elseif ($search == "nomornaskah") {
                $search1 = "AND (Nomor LIKE '%$q%' )";
            } else  {
                $search1 = "AND (JabatanPengirim LIKE '%$q%' or i.Instansipengirim like '%$q%') ";
                
            }
            // $search1 = "OR JabatanPengirim LIKE '%$q%' OR zz LIKE '%$q%' ";
            // echo $search1;die();
            if($sumber == "ternal"){
                $unit1 = " AND (rr.RoleId_From like '%$unit%')";
            } elseif($sumber == "internal"){
                $unit1 =" AND (rr.RoleId_From like '%$unit%')";
            } else{
                $unit1 = "";
            }
            // AND (Hal LIKE '%$q%' OR Nomor LIKE '%$q%' OR JabatanPengirim LIKE '%$q%' OR i.Instansipengirim like '%$q%')
            // "Hal LIKE '%$q%' OR Nomor LIKE '%$q%' JabatanPengirim LIKE '%$q%' OR zz LIKE '%$q%' OR rr.RoleId_From like '%$unit%'"
            // echo $unit1;die();
            // " AND (rr.RoleId_From = '$unit') ";
            // Query
            // $query = "
            //     SELECT SQL_CALC_FOUND_ROWS NId, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds
            //     FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            //     Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            //     i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz,
            //     (SELECT 'Approve' AS ds FROM db_sikd_mk.inbox_ds xx WHERE xx.penanda_tangan=71  AND xx.FileStatus='draf_DS' AND xx.NId=i.NId LIMIT 1) AS ds
            //     FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            //     AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL) 
            //     AND (rr.RoleId_To=  'uk.1.1.18')ORDER BY ReceiveDate DESC) a 
            //     ORDER BY ReceiveDate DESC
            //     LIMIT 
            //     {$begin}, {$row}
            // ";
            // echo $read;die(); JabatanPengim / zz
            $query = "
            SELECT SQL_CALC_FOUND_ROWS To_Id, RoleId_To,NId, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds, RoleId_From, Pengirim, JabatanPengirim
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive, rr.To_Id as To_Id,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,rr.RoleId_From AS RoleId_From, i.Instansipengirim AS zz, i.Pengirim AS Pengirim, i.JabatanPengirim AS JabatanPengirim,
            '-'AS ds
            FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (i.Pengirim LIKE'%$sumber%') 
            
            AND (rr.RoleId_To='$primaryroleid') 
            $unit1
            $read1 
            AND (rr.ReceiveDate >= $tgl1 AND rr.ReceiveDate <= $tgl2)
            
            AND (Hal LIKE '%$q%' OR Nomor LIKE '%$q%' OR JabatanPengirim LIKE '%$q%' OR i.Instansipengirim like '%$q%')
            ) a  
            ORDER BY ReceiveDate DESC
            
            
            LIMIT $page, $row
            
                
            ";
            // ORDER BY ReceiveDate $sort
            // $search1
            // echo $query;die();
            // AND (rr.RoleId_From = '$unit') 
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }

        // public function read_count(){
        //     $query = "SELECT FOUND_ROWS()";
        //     $stmt = $this->conn->prepare($query);
            
            
        //     //Execute query
        //     $stmt->execute();

        //     echo $stmt;die();
        // }

        public function read_count($begin, $row, $primaryroleid, $sumber, $read, $tgl1,$tgl2,$unit,$sort,$search,$q){
            $query = "
            SELECT SQL_CALC_FOUND_ROWS NId, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds, RoleId_From, Pengirim, JabatanPengirim
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,rr.RoleId_From AS RoleId_From, i.Instansipengirim AS zz, i.Pengirim AS Pengirim, i.JabatanPengirim AS JabatanPengirim,
            '-'AS ds
            FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (i.Pengirim LIKE'%$sumber%') 
            AND (rr.RoleId_To='$primaryroleid') 
            $unit1
            $read1 
            AND (rr.ReceiveDate >= $tgl1 AND rr.ReceiveDate <= $tgl2)
            $search1
            ORDER BY ReceiveDate ASC) a 
            
            ORDER BY ReceiveDate $sort
            ";
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            //Execute query
            $stmt->execute();
            return $stmt;
        }

        public function inbox_api($nid, $begin, $row, $primaryroleid, $sumber, $read, $tgl1,$tgl2,$unit,$sort,$search,$q){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            $page = ($begin*$row) - $row;
            // echo $begin; die();
            
            if($read == "read"){
                $read1 = 'AND (rr.StatusReceive="read")'  ;
                
            } elseif($read == "unread"){
                $read1 = 'AND (rr.StatusReceive="unread")'  ;
            } else {
                $read1 = ' ';
            }
            // echo $read1;die();
            if($search =="hal"){
                $search1 = "AND (Hal LIKE '%$q%' )";
            } else {
                $search1 = "AND (Nomor LIKE '%$q%' )";
            }
            if($sumber == "ternal"){
                $unit1 = " AND (rr.RoleId_From like '%$unit%')";
            } elseif($sumber == "internal"){
                $unit1 =" AND (rr.RoleId_From like '%$unit%')";
            } else{
                $unit1 = "";
            }
           
            $query = "
            SELECT SQL_CALC_FOUND_ROWS To_Id,GIR_Id, RoleId_To,NId, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds, RoleId_From, Pengirim, JabatanPengirim
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive, rr.To_Id AS To_Id,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,rr.RoleId_From AS RoleId_From, i.Instansipengirim AS zz, i.Pengirim AS Pengirim, i.JabatanPengirim AS JabatanPengirim,
            '-'AS ds, rr.GIR_Id AS GIR_Id
            FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId = '$nid')
            
            AND (i.Pengirim LIKE'%$sumber%') 
            
            AND (rr.RoleId_To='$primaryroleid') 
            $unit1
            $read1 
            AND (rr.ReceiveDate >= $tgl1 AND rr.ReceiveDate <= $tgl2)
            
            AND (Hal LIKE '%$q%' OR Nomor LIKE '%$q%' OR JabatanPengirim LIKE '%$q%' OR i.Instansipengirim like '%$q%')
            ) a  
            ORDER BY ReceiveDate DESC
            
            
            LIMIT $page, $row
            
                
            ";
            // echo $query;die();
            // AND (rr.RoleId_From = '$unit') 
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }

        public function inbox_api2($nid, $begin, $row, $primaryroleid, $sumber, $read, $tgl1,$tgl2,$unit,$sort,$search,$q){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            $page = ($begin*$row) - $row;
            // echo $begin; die();
            
            if($read == "read"){
                $read1 = 'AND (rr.StatusReceive="read")'  ;
                
            } elseif($read == "unread"){
                $read1 = 'AND (rr.StatusReceive="unread")'  ;
            } else {
                $read1 = ' ';
            }
            // echo $read1;die();
            if($search =="hal"){
                $search1 = "AND (Hal LIKE '%$q%' )";
            } else {
                $search1 = "AND (Nomor LIKE '%$q%' )";
            }
            if($sumber == "ternal"){
                $unit1 = " AND (rr.RoleId_From like '%$unit%')";
            } elseif($sumber == "internal"){
                $unit1 =" AND (rr.RoleId_From like '%$unit%')";
            } else{
                $unit1 = "";
            }
            // JOIN people p ON (p.PeopleId = rr.From_Id)
            //     JOIN people p2 ON (p.PeopleId = rr.To_Id)
            $query = "
            SELECT SQL_CALC_FOUND_ROWS To_Id,GIR_Id, RoleId_To,NId, NFileDir, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds, RoleId_From, Pengirim, JabatanPengirim
            FROM   (SELECT  i.NId AS NId, i.NFileDir as NFileDir,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.Tgl AS Tgl,i.NIsi AS NIsi,
            i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive, rr.To_Id AS To_Id, p.PeopleName ,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,rr.RoleId_From AS RoleId_From, i.Instansipengirim AS zz, i.Pengirim AS Pengirim, i.JabatanPengirim AS JabatanPengirim,
            '-'AS ds, rr.GIR_Id AS GIR_Id
            FROM ((
                inbox i JOIN inbox_receiver rr ON(
                    ((rr.NKey = i.NKey) AND (rr.NId = i.NId))
                )
                JOIN people p ON (p.PeopleId = i.Namapengirim)) 
                
                JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))
                
                ) WHERE (i.NId = '$nid')

            
            AND (i.Pengirim LIKE'%$sumber%') 
            
            AND (rr.RoleId_To='$primaryroleid') 
            $unit1
            $read1 
            AND (rr.ReceiveDate >= $tgl1 AND rr.ReceiveDate <= $tgl2)
            
            AND (Hal LIKE '%$q%' OR Nomor LIKE '%$q%' OR JabatanPengirim LIKE '%$q%' OR i.Instansipengirim like '%$q%')
            ) a  
            GROUP BY NId
            ORDER BY ReceiveDate DESC
            
            
            LIMIT $page, $row
            
                
            ";
            // echo $query;die();
            // AND (rr.RoleId_From = '$unit') 
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }

        public function getDisposisi($NId, $GIR_Id){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            // echo $NId $GIR_Id ;die();
            
            // Query
            $query = "
                SELECT (REPLACE(id.Disposisi, '|', '\',\'')) as Disposisi  
                FROM inbox_disposisi id 
                WHERE NId ='{$NId}' AND GIR_Id = '{$GIR_Id}'  
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }

        public function pesan_disposisi($iDisp){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            // echo $iDisp;die();
            
            // Query
            $query = "
            SELECT * FROM master_disposisi WHERE DisposisiId IN('{$iDisp}') ORDER BY urutan ASC, DisposisiName ASC    
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }
        
        // Getting inbox_receiver from DB
        public function readCount($begin, $row, $primaryroleid, $sumber, $read, $tgl1,$tgl2,$unit,$sort,$search,$q){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            $page = ($begin*$row) - $row;
            // echo $begin; die();
            
            if($read == "read"){
                $read1 = 'AND (rr.StatusReceive="read")'  ;
                
            } elseif($read == "unread"){
                $read1 = 'AND (rr.StatusReceive="unread")'  ;
            } else {
                $read1 = ' ';
            }
            // echo $read1;die();
            if($search =="hal"){
                $search1 = "AND (Hal LIKE '%$q%' )";
            } else {
                $search1 = "AND (Nomor LIKE '%$q%' )";
            }
            if($sumber == "ternal"){
                $unit1 = " AND (rr.RoleId_From like '%$unit%')";
            } elseif($sumber == "internal"){
                $unit1 =" AND (rr.RoleId_From like '%$unit%')";
            } else{
                $unit1 = "";
            }
            // echo $unit1;die();
            // " AND (rr.RoleId_From = '$unit') ";
            // Query
            // $query = "
            //     SELECT SQL_CALC_FOUND_ROWS NId, StatusReceive, Nomor, InstansiPengirim, Hal, Tgl, ReceiveDate, zz, ds
            //     FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            //     Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            //     i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz,
            //     (SELECT 'Approve' AS ds FROM db_sikd_mk.inbox_ds xx WHERE xx.penanda_tangan=71  AND xx.FileStatus='draf_DS' AND xx.NId=i.NId LIMIT 1) AS ds
            //     FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            //     AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL) 
            //     AND (rr.RoleId_To=  'uk.1.1.18')ORDER BY ReceiveDate DESC) a 
            //     ORDER BY ReceiveDate DESC
            //     LIMIT 
            //     {$begin}, {$row}
            // ";
            // echo $read;die();
            $query = "
            SELECT SQL_CALC_FOUND_ROWS NId
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,rr.RoleId_From AS RoleId_From, i.Instansipengirim AS zz, i.Pengirim AS Pengirim, i.JabatanPengirim AS JabatanPengirim,
            '-'AS ds
            FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (i.Pengirim LIKE'%$sumber%') 
            AND (rr.RoleId_To='$primaryroleid') 
            $unit1
            $read1 
            AND (rr.ReceiveDate >= $tgl1 AND rr.ReceiveDate <= $tgl2)
            $search1
            ORDER BY ReceiveDate ASC) a 
            
            ORDER BY ReceiveDate $sort
            
            
                
            ";
            // echo $query;die();
            // AND (rr.RoleId_From = '$unit') 
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            // echo $total_row;die();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }
        public function unitKerja(){
            $query="
            
            SELECT * FROM role  WHERE RoleStatus = 1 AND gjabatanId = 'XxJyPn38Yh.2' AND RoleId IN ('uk.1.2')
            UNION 
        SELECT * FROM role  WHERE RoleParentId = 'uk.1.1' AND RoleStatus = 1 AND gjabatanId = 'XxJyPn38Yh.3' ORDER BY CHAR_LENGTH(RoleId)
            ";
            
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            //Execute query
            $stmt->execute();
             
            // The Result send to API Endpoint on API folder
            return $stmt;
             
        }

        public function respon_unitKerja($RoleId, $tgl1, $tgl2){
            $query = "
            SELECT CONCAT( FLOOR(HOUR(b.wt) / 24), ' Hari ',MOD(HOUR(b.wt), 24), ' Jam ', MINUTE(b.wt), ' Menit ')  AS waktu_response, wt
                , TIME_TO_SEC(wt)AS wt2  FROM (
                    SELECT SEC_TO_TIME(AVG(TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate))) AS wt  FROM
                        ( SELECT * FROM inbox_receiver ir WHERE RoleId_From != '' AND (RoleId_To LIKE '$RoleId%')   AND  (ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                        ORDER BY readDate DESC) a ) b
            ";
            // echo $query;die();
             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }

        

        public function surat_masuk($RoleId){
            $query = "
            SELECT COUNT(*) AS surat_masuk
            FROM (SELECT  i.NId AS NId
		        FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                    AND (rr.NId = i.NId)))) ) WHERE (i.NId IS NOT NULL)
                    AND (rr.RoleId_To LIKE '$RoleId%' )  
                    AND (rr.StatusReceive = 'read' or rr.StatusReceive = 'unread')
                    AND (rr.ReceiveDate BETWEEN '2021-01-01 00:00:00' AND '2021-07-30 23:59:59')  
                    ORDER BY ReceiveDate DESC) a

            ";
            // $query = "
            // SELECT COUNT(*) AS surat_masuk
            //     FROM (SELECT rr.NId, rr.GIR_Id 
            //         FROM inbox_receiver rr 
            //         WHERE (rr.NKey ='XxJyPn38Yh') 
            //         AND rr.NId IS NOT NULL
            //         AND rr.NId NOT LIKE '.%'
            //         AND rr.NId NOT LIKE 'a%'
            //         AND rr.NId NOT LIKE 'b%'
            //         AND rr.NId NOT LIKE 'c%'
            //         AND rr.NId NOT LIKE 'j%'
            //         AND rr.NId NOT LIKE '%.'
            //         AND rr.NId NOT LIKE '%a'
            //         AND rr.NId NOT LIKE '%b'
            //         AND rr.NId NOT LIKE '%c'
            //         AND rr.NId NOT LIKE '%d'
            //         AND (rr.RoleId_To LIKE '$RoleId%' )  
            //         AND (rr.StatusReceive = 'read' or rr.StatusReceive = 'unread')
            //         AND (rr.ReceiveDate BETWEEN '2021-01-01 00:00:00' AND '2021-07-30 23:59:59')  
            //         ORDER BY ReceiveDate DESC) a

            // ";
//             $query = "
//             SELECT COUNT(*) AS surat_masuk
//             FROM (SELECT  i.NId AS NId , rr.GIR_Id AS GIR_Id, rr.RoleId_To AS RoleId_To
// 		FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
// 		    AND (rr.NId = i.NId)))) ) WHERE (i.NId IS NOT NULL)
// 		    AND (rr.RoleId_From LIKE '$RoleId%' )  
// 		    AND (rr.StatusReceive LIKE '%read%')
// 		    AND (rr.ReceiveDate BETWEEN '2021-06-01 00:00:00' AND '2021-07-30 23:59:59')  
// 		    ORDER BY ReceiveDate DESC) a
// UNION 
// SELECT COUNT(*) AS surat_keluar
// 	FROM (SELECT rr.NId, rr.GIR_Id 
// 		FROM inbox_receiver rr 
// 		WHERE (rr.NKey ='XxJyPn38Yh') 
// 		AND rr.NId IS NOT NULL
// 		AND rr.NId NOT LIKE '.%'
// 		AND rr.NId NOT LIKE 'a%'
// 		AND rr.NId NOT LIKE 'b%'
// 		AND rr.NId NOT LIKE 'c%'
// 		AND rr.NId NOT LIKE 'j%'
// 		AND rr.NId NOT LIKE '%.'
// 		AND rr.NId NOT LIKE '%a'
// 		AND rr.NId NOT LIKE '%b'
// 		AND rr.NId NOT LIKE '%c'
// 		AND rr.NId NOT LIKE '%d'
// 		AND (rr.RoleId_To LIKE '$RoleId%' )  
// 		AND (rr.ReceiveDate BETWEEN '2021-06-01 00:00:00' AND '2021-07-30 23:59:59')  
// 		ORDER BY ReceiveDate DESC) a	


//             ";

             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }

        public function surat_keluar($RoleId){
            $query = "
            SELECT COUNT(*) AS surat_keluar
            FROM (SELECT  i.NId AS NId
		        FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                    AND (rr.NId = i.NId)))) ) WHERE (i.NId IS NOT NULL)
                    AND (rr.RoleId_From LIKE '$RoleId%' )  
                    AND (rr.StatusReceive LIKE '%read%')
                    AND (rr.ReceiveDate BETWEEN '2021-06-01 00:00:00' AND '2021-07-30 23:59:59')  
                    ORDER BY ReceiveDate DESC) a

            ";
            // $query = "
            // SELECT COUNT(*) AS surat_keluar
            //     FROM (SELECT rr.NId, rr.GIR_Id 
            //         FROM inbox_receiver rr 
            //         WHERE (rr.NKey ='XxJyPn38Yh') 
            //         AND rr.NId IS NOT NULL
            //         AND rr.NId NOT LIKE '.%'
            //         AND rr.NId NOT LIKE 'a%'
            //         AND rr.NId NOT LIKE 'b%'
            //         AND rr.NId NOT LIKE 'c%'
            //         AND rr.NId NOT LIKE 'j%'
            //         AND rr.NId NOT LIKE '%.'
            //         AND rr.NId NOT LIKE '%a'
            //         AND rr.NId NOT LIKE '%b'
            //         AND rr.NId NOT LIKE '%c'
            //         AND rr.NId NOT LIKE '%d'
            //         AND (rr.RoleId_From LIKE '$RoleId%' )  
            //         AND (rr.StatusReceive LIKE '%read%')
            //         AND (rr.ReceiveDate BETWEEN '2021-01-01 00:00:00' AND '2021-07-30 23:59:59')  
            //         ORDER BY ReceiveDate DESC) a

            // ";

             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }

        public function jumlah_pegawai($RoleId){
            $query = "
                SELECT COUNT(*) AS jumlah_pegawai
                FROM (SELECT rr.PeopleName AS PeopleName
                    FROM people rr 
                    WHERE (rr.RoleAtasan LIKE '$RoleId%' or rr.PrimaryRoleId = '$RoleId')   
                    AND rr.PeopleIsActive= '1'
                    ORDER BY rr.PeopleId DESC) a
        
            ";

             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }





        public function spesifikPeople($q, $roleid){
            if(
                $roleid === 'uk.1.1' || $roleid === 'uk.1' || $roleid === 'uk.2' || $roleid === 'uk.3' || $roleid === 'uk.4' || $roleid === 'uk.5' || $roleid === 'uk.6' || $roleid === 'uk.7' || $roleid === 'uk.8' || $roleid === 'uk.9' || $roleid === 'uk.10' || $roleid === 'uk.11' || $roleid === 'uk.12' || $roleid === 'uk.1.1.18.26'  
            ){
                $r = '';
            } else {
                // $r = "
                //     AND RoleId !='uk.1.1' 
                //     AND RoleId !='uk.1'
                //     AND RoleId !='uk.2'
                //     AND RoleId !='uk.3'
                //     AND RoleId !='uk.4'
                //     AND RoleId !='uk.5'
                //     AND RoleId !='uk.6'
                //     AND RoleId !='uk.7'
                //     AND RoleId !='uk.8'
                //     AND RoleId !='uk.9'
                //     AND RoleId !='uk.10'
                //     AND RoleId !='uk.12'
                // ";
                $r = "
                    AND PrimaryRoleId !='uk.1.1' 
                    AND PrimaryRoleId !='uk.1'
                    AND PrimaryRoleId !='uk.2'
                    AND PrimaryRoleId !='uk.3'
                    AND PrimaryRoleId !='uk.4'
                    AND PrimaryRoleId !='uk.5'
                    AND PrimaryRoleId !='uk.6'
                    AND PrimaryRoleId !='uk.7'
                    AND PrimaryRoleId !='uk.8'
                    AND PrimaryRoleId !='uk.9'
                    AND PrimaryRoleId !='uk.10'
                    AND PrimaryRoleId !='uk.12'
                ";
            }
            // $query="
            // SELECT  PeopleId, PrimaryRoleId , CONCAT (PeopleName,' (',PeoplePosition,')') AS label 
            // FROM people 
            // WHERE (PeopleName LIKE '%$q%' OR PeoplePosition LIKE '%$q%')
            // AND PeopleIsActive = 1 
            // AND GroupId = 3 
            // AND PeopleName IS NOT NULL 
            
            // AND PrimaryRoleId !='uk' AND PrimaryRoleId !='root'
         
            // ORDER BY  CHAR_LENGTH(PrimaryRoleId) ASC


            // ";
            // $query="

            // SELECT  PeopleId, PrimaryRoleId , CONCAT (PeopleName,' (',PeoplePosition,')') AS label 
            //     FROM people WHERE  CONCAT (PeopleName,' (',PeoplePosition,')') 
            //     LIKE '%$q%' AND PeopleIsActive = 1 AND GroupId = 3 
            //     AND PeopleName IS NOT NULL 
            //     AND PrimaryRoleId !='uk' AND PrimaryRoleId !='root'
            //     $r 
            //     ORDER BY  CHAR_LENGTH(PrimaryRoleId) ASC


            // ";
            // $query="
            // SELECT RoleId, RoleName, CONCAT(RoleName , ' - ' , p.PeopleName)AS NAME FROM role 
            // JOIN people p ON p.PrimaryRoleId = RoleId
            // WHERE RoleName LIKE'%$q%'
            // AND RoleId !='root' 
            // AND RoleId !='uk'
            // $r
            // AND RoleStatus = '1' 
            // AND p.PeopleIsActive = '1'
            // AND p.RoleAtasan !=''
            // GROUP BY RoleId
            // ";

            // $query = "
            // SELECT CONCAT(PeopleName , ' ( ', PeoplePosition, ' ) ')AS label , PeopleId, PrimaryRoleId, PeopleName, PeoplePosition
            // FROM(SELECT PeopleId, PeopleName, PeoplePosition, PrimaryRoleId, RoleAtasan, GroupId FROM people p 
            // WHERE (p.PeopleName LIKE '%$q%' OR p.PeoplePosition LIKE '%$q%')
            // AND p.PeopleIsActive = '1' AND p.PrimaryRoleId != 'root' AND p.RoleAtasan != '' AND p.PrimaryRoleId != 'uk'
            // AND p.GroupId != '4' AND p.GroupId != '5'
            // AND p.PeopleId != '149' 
            // $r
            // )a
            // ORDER BY  CHAR_LENGTH(PrimaryRoleId) ASC
            // ";

            $query = "
            SELECT CONCAT(p.PeopleName, ' - ', p.PeoplePosition)AS label, p.PrimaryRoleId, p.PeopleName, p.PeoplePosition FROM role r 
            JOIN people p ON p.PrimaryRoleId = r.RoleId 
            WHERE (p.PeopleName LIKE '%$q%' OR p.PeoplePosition LIKE '%$q%')
            AND p.PeopleIsActive = '1' AND p.PrimaryRoleId != 'root' 
            AND p.RoleAtasan != '' AND p.PrimaryRoleId != 'uk'
            AND r.RoleStatus = '1' AND p.PeopleId != '149'
            AND p.GroupId != '4' AND p.GroupId != '5' 
            $r
            ORDER BY  CHAR_LENGTH(p.PrimaryRoleId) ASC

            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function listPegawai($unitkerja){
            
            if($unitkerja == 'uk.1.1'){
                $parentid = "AND r.RoleParentId = '$unitkerja' ";
                $groupid = "AND p.GroupId=3";
            } else {
                $parentid  =  "AND (p.RoleAtasan like '$unitkerja%' OR r.RoleId = '$unitkerja') ";
                $groupid = "AND (p.GroupId=3 OR p.GroupId=4 OR p.GroupId=5 OR p.GroupId=6)";
            }
            // echo $groupid;die();
            $query="
            SELECT r.RoleId, p.RoleAtasan , r.RoleName, r.RoleDesc, p.PeopleName
            FROM role r 
            JOIN people p ON p.PrimaryRoleId = r.RoleId
            WHERE r.RoleName LIKE '%%' 
            AND r.RoleStatus=1 
            $parentid 
            AND p.PeopleIsActive=1
            $groupid

            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function findNip($nip){
            $query="
            SELECT * , REPLACE(p.no_nip , ' ', '')AS NIP FROM people p
            JOIN role r ON r.RoleId = p.PrimaryRoleId
            WHERE 
            REPLACE(p.no_nip, ' ', '') = '$nip'
            AND p.PeopleIsActive = 1 AND p.GroupId = 3 
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function inbox_receiver($NId){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
           
            // echo $NId; die();
            // Query
            $query = 'SELECT 
            ir.NId, 
            ir.GIR_Id, 
            ir.From_Id, 
            p.PeopleName AS pengirim, 
            p2.PeopleName AS penerima,
            p2.PeoplePosition AS jabatan_penerima,
            r.RoleName AS jabatan_pengirim,
            ir.To_Id, 
            ir.RoleId_From, 
            ir.RoleId_To,
            i.NId, i.NTglReg, 
            ir.ReceiveDate,  
            ir.ReceiverAs,
            ir.readDate, 
            r.gjabatanId,
            ir.Msg,
            CASE 
                 WHEN ReceiverAs = "cc1" THEN "Disposisi" 
                 WHEN ReceiverAs = "to" THEN "Naskah Masuk Eksternal"
                 WHEN ReceiverAs = "to_t1" THEN "Naskah Tanpa Tindaklanjut"
                 WHEN ReceiverAs = "to_Konsep" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_notadinas" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_memo" THEN "Memo"
                 WHEN ReceiverAs = "to_forward"  THEN "Teruskan"
                 WHEN ReceiverAs = "to_reply" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usulan"  THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usul" THEN "Nota Dinas"
                 WHEN ReceiverAs = "bcc"  THEN "Tembusan"
                 WHEN ReceiverAs = "to_keluar" THEN "Konsep Naskah Keluar"
                 ELSE "Else dari nodin"
             END AS ReceiverAs ,
             CASE 
                WHEN JenisName = "3. Surat Undangan" THEN "Surat Undangan"
                WHEN JenisName = "1. Surat Dinas" THEN "Surat Dinas"
                WHEN JenisName = "6. Keputusan" THEN "Keputusan"
                WHEN JenisName = "5. Peraturan" THEN "Peraturan"
                WHEN JenisName = "2. Nota Dinas" THEN "Nota Dinas"
                WHEN JenisName = "4. Surat Tugas" THEN "Surat Tugas"
                WHEN JenisName = "7. Surat Edaran" THEN "Surat Edaran"

            END AS JenisName,

            CASE 
                WHEN r.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPengirim,

            CASE 
                WHEN r2.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r2.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r2.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r2.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r2.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r2.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r2.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r2.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r2.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r2.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r2.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPenerima,
            CONCAT( FLOOR(HOUR(TIMEDIFF(ir.ReceiveDate,ir.readDate)) / 24), " Hari ",
            MOD(HOUR(TIMEDIFF(ReceiveDate,readDate)), 24), " Jam ",
            MINUTE(TIMEDIFF(ReceiveDate,readDate)), " Menit ") AS Rate

             FROM ((((((inbox_receiver ir 
            JOIN inbox i ON i.NId = ir.NId )
            JOIN master_jnaskah mj ON mj.JenisId = i.JenisId)
            JOIN people p ON p.PeopleId = ir.From_Id)
            JOIN people p2 ON p2.PeopleId = ir.To_Id)
            JOIN role r ON r.RoleId = ir.RoleId_From)
            JOIN role r2 ON r2.RoleId = ir.RoleId_To)



             WHERE ir.NId = "' .$NId .'"
             ORDER BY ReceiveDate ASC
             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        public function inbox_receiver2($NId){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
           
            // echo $NId; die();
            // Query
            $query = 'SELECT 
            ir.NId, 
            ir.GIR_Id, 
            ir.From_Id, 
            p.PeopleName AS pengirim, 
            p2.PeopleName AS penerima,
            p2.PeoplePosition AS jabatan_penerima,
            r.RoleName AS jabatan_pengirim,
            ir.To_Id, 
            ir.RoleId_From, 
            ir.RoleId_To,
            p.pgw_id as RoleId_From_pgw_id,
            p.pgw_foto as RoleId_From_pgw_foto,
            p.no_nip as RoleId_From_no_nip,
            p2.pgw_id as RoleId_To_pgw_id,
            p2.pgw_foto as RoleId_To_pgw_foto,
            p2.no_nip as RoleId_To_no_nip,
            i.NId, i.NTglReg, 
            ir.ReceiveDate,  
            ir.ReceiverAs,
            ir.readDate, 
            r.gjabatanId,
            ir.Msg,
            CASE 
                 WHEN ReceiverAs = "cc1" THEN "Disposisi" 
                 WHEN ReceiverAs = "to" THEN "Naskah Masuk Eksternal"
                 WHEN ReceiverAs = "to_t1" THEN "Naskah Tanpa Tindaklanjut"
                 WHEN ReceiverAs = "to_Konsep" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_notadinas" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_memo" THEN "Memo"
                 WHEN ReceiverAs = "to_forward"  THEN "Teruskan"
                 WHEN ReceiverAs = "to_reply" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usulan"  THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usul" THEN "Nota Dinas"
                 WHEN ReceiverAs = "bcc"  THEN "Tembusan"
                 WHEN ReceiverAs = "to_keluar" THEN "Konsep Naskah Keluar"
                 ELSE "Else dari nodin"
             END AS ReceiverAs ,
             CASE 
                WHEN JenisName = "3. Surat Undangan" THEN "Surat Undangan"
                WHEN JenisName = "1. Surat Dinas" THEN "Surat Dinas"
                WHEN JenisName = "6. Keputusan" THEN "Keputusan"
                WHEN JenisName = "5. Peraturan" THEN "Peraturan"
                WHEN JenisName = "2. Nota Dinas" THEN "Nota Dinas"
                WHEN JenisName = "4. Surat Tugas" THEN "Surat Tugas"
                WHEN JenisName = "7. Surat Edaran" THEN "Surat Edaran"

            END AS JenisName,

            CASE 
                WHEN r.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPengirim,

            CASE 
                WHEN r2.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r2.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r2.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r2.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r2.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r2.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r2.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r2.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r2.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r2.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r2.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPenerima,
            CONCAT( FLOOR(HOUR(TIMEDIFF(ir.ReceiveDate,ir.readDate)) / 24), " Hari ",
            MOD(HOUR(TIMEDIFF(ReceiveDate,readDate)), 24), " Jam ",
            MINUTE(TIMEDIFF(ReceiveDate,readDate)), " Menit ") AS Rate

             FROM ((((((inbox_receiver ir 
            JOIN inbox i ON i.NId = ir.NId )
            JOIN master_jnaskah mj ON mj.JenisId = i.JenisId)
            JOIN people p ON p.PeopleId = ir.From_Id)
            JOIN people p2 ON p2.PeopleId = ir.To_Id)
            JOIN role r ON r.RoleId = ir.RoleId_From)
            JOIN role r2 ON r2.RoleId = ir.RoleId_To)



             WHERE ir.NId = "' .$NId .'"
             ORDER BY ReceiveDate ASC
             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        function inbox_files($NFileDir, $GIR_Id){
            // echo $NFileDir, $GIR_Id; die();
            // $tmpDir = "FilesUploaded/'$NFileDir'/"
            $query = "
                select concat('https://sikd.mkri.id/FilesUploaded/', '$NFileDir', '/', infs.FileName_fake) as FileDisk,
                    infs.FileName_real
                    FROM inbox_files infs
                        where infs.GIR_Id = '$GIR_Id'
                        order by infs.EditedDate DESC
            ";
            // echo $query;die();
            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }
        function inbox_ds($NFileDir, $NId, $GIR_Id){
            
            $query = "
            SELECT CONCAT('https://sikd.mkri.id/FilesUploaded_sd_ttd/', '$NFileDir', '/', file_ds) as FileDS, FileName_fake, FileName_real,file_ds, STATUS 
            FROM inbox_ds i WHERE  i.NId = '$NId' 
            AND i.GIR_Id = '$GIR_Id'
                
            ";
            
            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }
        function inbox_draft_ds($NFileDir, $NId, $GIR_Id){
            
            $query = "
            SELECT CONCAT('https://sikd.mkri.id/FilesUploaded_sd_ttd/', '$NFileDir', '/', file_ds) as FileDS, FileName_fake, FileName_real,file_ds, STATUS 
            FROM inbox_ds i WHERE  i.NId = '$NId' 
            AND i.GIR_Id = '$GIR_Id'
                
            ";
            
            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        function create_penting($nid, $peopleid, $primaryroleid, $date, $penting_id, $hal, $instansipengirim, $jabatanpengirim,$nomor, $pengirim, $receivedate, $statusreceive, $tgl, $zz, $ds)
        {
            
            // echo $result;die();
            $query = "
                INSERT INTO penting SET 
                penting_id='$penting_id',
                NId='$nid',
                PeopleId='$peopleid',
                PrimaryRoleId='$primaryroleid',
                created_date='$date',
                Hal='$hal',
                InstansiPengirim='$instansipengirim',
                JabatanPengirim='$jabatanpengirim',
                Nomor='$nomor',
                Pengirim='$pengirim',
                ReceiveDate='$receivedate',
                StatusReceive='$statusreceive',
                Tgl='$tgl',
                zz='$zz',
                ds='$ds'
            ";
            // echo $query;die();
            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            if($stmt->execute()){
                return $stmt;
             }
             return $stmt;
            // The Result send to API Endpoint on API folder
        }

        function delete_penting($penting_id){
            $query = "
                DELETE FROM penting WHERE penting_id='$penting_id'
            ";
            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            if($stmt->execute()){
                return $stmt;
            }
            return $stmt;
            
        }
        public function get_penting($peopleid, $primaryroleid){
            $query="
            SELECT * FROM penting p WHERE p.PeopleId='$peopleid' OR p.PrimaryRoleId='$primaryroleid' order by p.created_date DESC 
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }
        public function getId_penting($penting_id){
            $query="
            SELECT * FROM penting p WHERE p.penting_id='$penting_id' 
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function update_toread($NId, $GIR_Id, $To_Id, $RoleId_To, $readDate){
            $query = "
            UPDATE inbox_receiver 
                SET StatusReceive ='read', readDate = '$readDate'
                WHERE NKey='XxJyPn38Yh' 
                AND NId = '$NId' AND GIR_Id = '$GIR_Id' 
                AND To_Id = '$To_Id' 
                AND RoleId_To='$RoleId_To'
            ";
            
            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            if($stmt->execute()){
                return true;
             }
             return false;
        }

        public function list_unitkerja(){
            $query = "
            SELECT * FROM role  WHERE RoleStatus = 1 AND gjabatanId = 'XxJyPn38Yh.2' AND RoleId IN ('uk.1.2')
                UNION 
            SELECT * FROM role  WHERE RoleParentId = 'uk.1.1' AND RoleStatus = 1 AND gjabatanId = 'XxJyPn38Yh.3' ORDER BY CHAR_LENGTH(RoleId)
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
    
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function list_pegawai_unitkerja($RoleId){
            $query = "
            SELECT * FROM people WHERE PrimaryRoleId = '$RoleId' AND PeopleIsActive = '1' AND RoleAtasan IN ('uk.1.1', 'uk.1') AND GroupId = '3'
                UNION
            SELECT * FROM people WHERE RoleAtasan LIKE '$RoleId%' AND PeopleIsActive = '1' and GroupId != '5' and GroupId != '4'
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
    
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function data_pemakaian_unitkerja(){
            $query = "
            
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
    
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

        public function respon_pegawai_unitkerja($PrimaryRoleId, $tgl1, $tgl2){
            $query = "
            SELECT CONCAT( FLOOR(HOUR(b.wt) / 24), ' Hari ',MOD(HOUR(b.wt), 24), ' Jam ', MINUTE(b.wt), ' Menit ')  AS waktu_response, wt
                , TIME_TO_SEC(wt)AS wt2  FROM (
                    SELECT SEC_TO_TIME(AVG(TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate))) AS wt  FROM
                        ( SELECT * FROM inbox_receiver ir WHERE RoleId_From != '' AND (RoleId_To = '$PrimaryRoleId')   AND  (ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                        ORDER BY readDate DESC) a ) b
            ";
            // echo $query;die();
             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }

        public function respon_pegawai_unitkerja2($PrimaryRoleId, $tgl1, $tgl2){
            $query = "
            SELECT CONCAT( FLOOR(HOUR(b.wt) / 24), ' Hari ',MOD(HOUR(b.wt), 24), ' Jam ', MINUTE(b.wt), ' Menit ')  AS waktu_response, wt
                , TIME_TO_SEC(wt)AS wt2  FROM (
                    SELECT SEC_TO_TIME(AVG(TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate))) AS wt  FROM
                        ( SELECT * FROM inbox_receiver ir WHERE RoleId_From != '' AND (RoleId_To = '$PrimaryRoleId')   AND  (ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                        ORDER BY readDate DESC) a ) b
            ";
            // echo $query;die();
             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }


        

        public function surat_masuk_pegawai_unitkerja($PrimaryRoleId, $tgl1,$tgl2){
            $query = "
            SELECT COUNT(*) AS surat_masuk
            FROM (SELECT  i.NId AS NId
		        FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                    AND (rr.NId = i.NId)))) ) WHERE (i.NId IS NOT NULL)
                    AND (rr.RoleId_To ='$PrimaryRoleId' )  
                    AND (rr.StatusReceive = 'read' or rr.StatusReceive = 'unread')
                    AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')  
                    ORDER BY ReceiveDate DESC) a

            ";
            

             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }

        public function surat_keluar_pegawai_unitkerja($PrimaryRoleId, $tgl1,$tgl2){
            $query = "
            SELECT COUNT(*) AS surat_keluar
            FROM (SELECT  i.NId AS NId
		        FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                    AND (rr.NId = i.NId)))) ) WHERE (i.NId IS NOT NULL)
                    AND (rr.RoleId_From = '$PrimaryRoleId' )  
                    AND (rr.StatusReceive LIKE '%read%')
                    AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')  
                    ORDER BY ReceiveDate DESC) a

            ";

             //Prepare statement
             $stmt = $this->conn->prepare($query);
            
             //Execute query
             $stmt->execute();
              
             // The Result send to API Endpoint on API folder
             return $stmt;
        }
        public function desc_role($RoleId){
            $query = "
                SELECT * FROM role 
                JOIN people p ON p.PrimaryRoleId = '$RoleId' AND p.PeopleIsActive = '1' AND p.RoleAtasan != '' AND p.GroupId IN('2', '3')
                WHERE RoleId = '$RoleId' 

            ";
            //Prepare statement
            $stmt = $this->conn->prepare($query);
        
            //Execute query
            $stmt->execute();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
        }

       


    }



?>