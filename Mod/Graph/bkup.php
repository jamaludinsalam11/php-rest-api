<?php

include_once '../../utilities/checkRequestMethod.php';

    Class Graph {
        //DB Stuff
        private $conn;
        private $table= 'inbox_receiver';

        //Inbox Receiver Properties
        public $NId;
        public $GIR_Id;
        public $From_Id;
        public $RoleId_From;
        public $To_Id;
        public $RoleId_To;
        public $ReceiverAs;
        public $Msg;
        public $StatusReceive;
        public $ReceiveDate;
        public $DisposisiId;
        public $DisposisiName;
        public $gjabatanId;
        public $urutan;


        // Constructor with DB
        public function __construct($db){
            $this->conn = $db;
        }

         // Getting inbox_receiver from DB
         public function read($NId){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
           
            // echo $NId; die();
            // Query
            $query = 'SELECT 
            ir.NId, 
            ir.GIR_Id, 
            ir.From_Id, 
            p.PeopleName AS pengirim, 
            p2.PeopleName AS penerima,
            p2.PeoplePosition AS jabatan_penerima,
            r.RoleName AS jabatan_pengirim,
            ir.To_Id, 
            ir.RoleId_From, 
            i.NId, i.NTglReg, 
            ir.ReceiveDate,  
            ir.ReceiverAs,
            ir.readDate, 
            r.gjabatanId,
            (SELECT REPLACE(id.Disposisi, "|", "\',\'"))  AS pesan_disposisi, 
            ir.Msg,
            id.Sifat  ,
            CASE 
                 WHEN ReceiverAs = "cc1" THEN "Disposisi" 
                 WHEN ReceiverAs = "to" THEN "Naskah Masuk Eksternal"
                 WHEN ReceiverAs = "to_t1" THEN "Naskah Tanpa Tindaklanjut"
                 WHEN ReceiverAs = "to_Konsep" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_notadinas" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_memo" THEN "Memo"
                 WHEN ReceiverAs = "to_forward"  THEN "Teruskan"
                 WHEN ReceiverAs = "to_reply" THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usulan"  THEN "Nota Dinas"
                 WHEN ReceiverAs = "to_usul" THEN "Nota Dinas"
                 WHEN ReceiverAs = "bcc"  THEN "Tembusan"
                 WHEN ReceiverAs = "to_keluar" THEN "Konsep Naskah Keluar"
                 ELSE "Else dari nodin"
             END AS ReceiverAs ,
             CASE 
                WHEN JenisName = "3. Surat Undangan" THEN "Surat Undangan"
                WHEN JenisName = "1. Surat Dinas" THEN "Surat Dinas"
                WHEN JenisName = "6. Keputusan" THEN "Keputusan"
                WHEN JenisName = "5. Peraturan" THEN "Peraturan"
                WHEN JenisName = "2. Nota Dinas" THEN "Nota Dinas"
                WHEN JenisName = "4. Surat Tugas" THEN "Surat Tugas"
                WHEN JenisName = "7. Surat Edaran" THEN "Surat Edaran"

            END AS JenisName,

            CASE 
                WHEN r.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPengirim,

            CASE 
                WHEN r2.gjabatanId ="XxJyPn38Yh.1" THEN "Pimpinan Instansi"
                WHEN r2.gjabatanId ="XxJyPn38Yh.2" THEN "Eselon I"
                WHEN r2.gjabatanId ="XxJyPn38Yh.3" THEN "Eselon II"
                WHEN r2.gjabatanId ="XxJyPn38Yh.4" THEN "Eselon III"
                WHEN r2.gjabatanId ="XxJyPn38Yh.5" THEN "Eselon IV"
                WHEN r2.gjabatanId ="XxJyPn38Yh.6" THEN "Unit Kearsipan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.13" THEN "Fungsional Umum"
                WHEN r2.gjabatanId ="XxJyPn38Yh.8" THEN "Pengelola Surat"
                WHEN r2.gjabatanId ="XxJyPn38Yh.9" THEN "Eselon V"
                WHEN r2.gjabatanId ="XxJyPn38Yh.15" THEN "Sekretaris Pimpinan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.11" THEN "Pengelola Keuangan"
                WHEN r2.gjabatanId ="XxJyPn38Yh.12" THEN "Fungsional Tertentu"
                WHEN r2.gjabatanId ="XxJyPn38Yh.14" THEN "Panitera Pengganti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.16" THEN "Peneliti"
                WHEN r2.gjabatanId ="XxJyPn38Yh.17" THEN "Auditor"
                WHEN r2.gjabatanId ="XxJyPn38Yh.18" THEN "Pranata Komputer"
                
            END AS JabatanPenerima,
            CONCAT( FLOOR(HOUR(TIMEDIFF(ir.ReceiveDate,ir.readDate)) / 24), " Hari ",
            MOD(HOUR(TIMEDIFF(ReceiveDate,readDate)), 24), " Jam ",
            MINUTE(TIMEDIFF(ReceiveDate,readDate)), " Menit ") AS Rate

             FROM (((((((inbox_receiver ir 
            JOIN inbox i ON i.NId = ir.NId )
            JOIN master_jnaskah mj ON mj.JenisId = i.JenisId)
            JOIN people p ON p.PeopleId = ir.From_Id)
            JOIN people p2 ON p2.PeopleId = ir.To_Id)
            JOIN role r ON r.RoleId = ir.RoleId_From)
            JOIN role r2 ON r2.RoleId = ir.RoleId_To)
            JOIN inbox_disposisi id ON id.NId=ir.NId AND id.GIR_Id=ir.GIR_Id)



             WHERE ir.NId = "' .$NId .'"
             ORDER BY ReceiveDate ASC
             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
         }
        
         public function pesan_disposisi($iDisp){

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            
            // echo $iDisp;die();
            
            // Query
            $query = "
            SELECT * FROM master_disposisi WHERE DisposisiId IN('{$iDisp}') ORDER BY urutan ASC, DisposisiName ASC    
            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);
            
            
            //Execute query
            $stmt->execute();

                       
            // $total_row = $stmt->rowCount();
            
            // The Result send to API Endpoint on API folder
            return $stmt;
            
        }

    }



?>