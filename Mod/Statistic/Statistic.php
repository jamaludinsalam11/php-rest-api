<?php

include_once '../../utilities/checkRequestMethod.php';

    Class Statistic {
        //DB Stuff
        private $conn;
        private $table= 'inbox_receiver';

        //Inbox Receiver Properties
        // public $NId;
        // public $GIR_Id;
        // public $From_Id;
        // public $RoleId_From;
        // public $To_Id;
        // public $RoleId_To;
        // public $ReceiverAs;
        // public $Msg;
        // public $StatusReceive;
        // public $ReceiveDate;
        // public $DisposisiId;
        // public $DisposisiName;
        // public $gjabatanId;
        // public $urutan;
        public $total_ttde;


        // Constructor with DB
        public function __construct($db){
            $this->conn = $db;
        }

        // TOTAL TANDA TANGAN ELEKTRONIK
        public function total_ttde($tgl1, $tgl2, $select_id) {
            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = "
            SELECT COUNT(*) as total_ttde FROM (
                SELECT a.NId, a.GIR_Id, a.penanda_tangan FROM  inbox_ds a  
                 LEFT JOIN inbox_receiver b ON a.NId = b.NId WHERE a.FileStatus ='available' AND (b.ReceiveDate BETWEEN '$tgl1' AND '$tgl2') GROUP BY a.GIR_Id)c 
                 LEFT JOIN people d ON c.penanda_tangan = d.PeopleId WHERE d.PrimaryRoleId ='$select_id'
       
             ";
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        public function all_ttde($tgl1, $tgl2, $select_id){
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          $query = "
          SELECT COUNT(*) AS all_ttde FROM (
            SELECT a.NId, a.GIR_Id, a.penanda_tangan FROM  inbox_ds a  
             LEFT JOIN inbox_receiver b ON a.NId = b.NId WHERE a.FileStatus ='available' AND (b.ReceiveDate BETWEEN '$tgl1' AND '$tgl2') GROUP BY a.GIR_Id)c 
             LEFT JOIN people d ON c.penanda_tangan = d.PeopleId WHERE d.PrimaryRoleId like 'uk.1.%'
          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

         
        public function waktu_response($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT CONCAT( FLOOR(HOUR(b.wt) / 24), " Hari ",MOD(HOUR(b.wt), 24), " Jam ", MINUTE(b.wt), " Menit ")  AS waktu_response, wt
            , TIME_TO_SEC(wt)AS wt2  FROM (
                SELECT SEC_TO_TIME(AVG(TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate))) AS wt FROM
                    ( SELECT * FROM inbox_receiver WHERE RoleId_From != "" AND RoleId_To ="'. $select_id . '"  AND  (ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")
                 ORDER BY readDate DESC) a ) b
             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        public function average_response($tgl1, $tgl2) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT  CONCAT( FLOOR(HOUR(c.wt) / 24), " Hari ",MOD(HOUR(c.wt), 24), " Jam ", MINUTE(c.wt), " Menit ")  AS average_response, wt ,
            TIME_TO_SEC(wt) AS wt2
            FROM (SELECT SEC_TO_TIME(AVG(Rate)) AS wt FROM (
                SELECT a.NId,a.readDate, TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate) AS Rate FROM
                ( SELECT * FROM inbox_receiver WHERE ReceiveDate!= "" AND readDate != "" AND RoleId_From != ""  AND  (ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")
              ORDER BY readDate DESC) a ) b) c
             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }


        // Pesan Belum Dibaca
        public function belum_dibaca($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = "
            SELECT * 
                FROM   (SELECT  i.NId AS NId,i.Hal AS Hal,i.
                Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
                i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz , 
                CASE 
                 WHEN rr.ReceiverAs = 'cc1' THEN 'Disposisi'
                 WHEN rr.ReceiverAs = 'to' THEN 'Naskah Masuk Eksternal'
                 WHEN rr.ReceiverAs = 'to_t1' THEN 'Naskah Tanpa Tindaklanjut'
                 WHEN rr.ReceiverAs = 'to_Konsep' THEN 'Nota Dinas'
                 WHEN rr.ReceiverAs = 'to_notadinas' THEN 'Nota Dinas'
                 WHEN rr.ReceiverAs = 'to_memo' THEN 'Memo'
                 WHEN rr.ReceiverAs = 'to_forward'  THEN 'Teruskan'
                 WHEN rr.ReceiverAs = 'to_reply' THEN 'Nota Dinas'
                 WHEN rr.ReceiverAs = 'to_usulan'  THEN 'Nota Dinas'
                 WHEN rr.ReceiverAs = 'to_usul' THEN 'Nota Dinas'
                 WHEN rr.ReceiverAs = 'bcc'  THEN 'Tembusan'
                 WHEN rr.ReceiverAs = 'to_keluar' THEN 'Konsep Naskah Keluar'
                 ELSE 'Else dari nodin'
                END AS ReceiverAs 
                
                FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
                AND (rr.RoleId_To='$select_id' )  AND (rr.StatusReceive = 'unread')AND (rr.ReceiveDate BETWEEN '$tgl1' AND '$tgl2')  ORDER BY ReceiveDate DESC) a
           

            " ;
            // $query = '
            // SELECT COUNT(*) as belum_dibaca
            //     FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            //     Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            //     i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            //     AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            //     AND (rr.RoleId_To="'. $select_id . '" )  AND (rr.StatusReceive = "unread")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a

            //  ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Pesan Sudah Dibaca
        public function sudah_dibaca($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*) as sudah_dibaca
                FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
                Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
                i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
                AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
                AND (rr.RoleId_To="'. $select_id . '" )  AND (rr.StatusReceive = "read")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a

             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Pesan Sudah Dibaca
        public function pesan_masuk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as sudah_dibaca
              FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
              Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
              i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
              AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
              AND (rr.RoleId_To='$select_id' )  AND (rr.StatusReceive like '%read%')AND (rr.ReceiveDate BETWEEN '$tgl1' AND '$tgl2' )  ORDER BY ReceiveDate DESC) a

           ";
          // 412.230719090222 

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }


         // Sumber External
         public function sumber_external($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '

            SELECT COUNT(*) as sumber_external
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To="'. $select_id . '")  AND (i.instansipengirim NOT LIKE "uk.%")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a
            

             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Sumber Internal
        public function sumber_internal($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '

            SELECT COUNT(*) as sumber_internal
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To="'. $select_id . '")  AND (i.instansipengirim LIKE "uk.%")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a
            

             ';
            // 412.230719090222 

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Nota Dinas Masuk
        public function notadinas_masuk($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*) as notadinas_masuk
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To="'. $select_id . '")  AND (rr.ReceiverAs IN (  "to" ,"to_konsep","to_keluar",  "to_usul",  "to_reply", "to_forward"))
            AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a


            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Nota Dinas ALL Masuk
        public function notadinas_masuk_all_unit($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();

          // echo($select_id);die();

            // Query
            
          $query = "
          SELECT COUNT(*) AS notadinas_masuk_all_unit
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_To like '$select_id%' )  AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar', 'to_usul',  'to_reply', 'to_forward'))
          AND (rr.ReceiveDate BETWEEN '$tgl1'  AND '$tgl2')  ORDER BY ReceiveDate DESC) a
          ";

          //Prepare statement
          // echo $query;die();
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

         // Nota Dinas ALL Masuk
         public function notadinas_masuk_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();

          // echo($select_id);die();

            // Query
            
          $query = "
          SELECT COUNT(*) AS notadinas_masuk_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr 
            ON(( (rr.NKey = i.NKey) AND (rr.NId = i.NId) ) )) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_To like 'uk.1%' )  AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar', 'to_usul',  'to_reply', 'to_forward'))
          AND (rr.ReceiveDate BETWEEN '$tgl1'  AND '$tgl2')   ORDER BY ReceiveDate DESC) a 
          ";

          //Prepare statement
          // echo $query;die();
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

        

        // Disposisi Masuk
        public function disposisi_masuk($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*)as disposisi_masuk
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To="'. $select_id . '")  AND (rr.ReceiverAs = "cc1")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a
           

            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Disposisi Masuk All Unit
        public function disposisi_masuk_all_unit($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = "
            SELECT COUNT(*)AS disposisi_masuk_all_unit
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To like '$select_id%')  AND (rr.ReceiverAs = 'cc1')AND (rr.ReceiveDate BETWEEN '$tgl1'  AND '$tgl2')  ORDER BY ReceiveDate DESC) a

            ";

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Disposisi Masuk All Unit
        public function disposisi_masuk_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*)AS disposisi_masuk_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_To like 'uk.1.%')  AND (rr.ReceiverAs = 'cc1')AND (rr.ReceiveDate BETWEEN '$tgl1'  AND '$tgl2')  ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

        // Tembusan Masuk
        public function tembusan_masuk($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*)AS tembusan_masuk
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_To="'. $select_id . '")  AND (rr.ReceiverAs = "bcc") AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")
            ORDER BY ReceiveDate DESC) a

            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Tembusan Masuk
        public function tembusan_masuk_all_unit($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*)AS tembusan_masuk_all_unit
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_To like '$select_id%')  AND (rr.ReceiverAs = 'bcc') AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')
          ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
      }
         // Tembusan Masuk All MK
         public function tembusan_masuk_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*)AS tembusan_masuk_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_To like 'uk.1.1%')  AND (rr.ReceiverAs = 'bcc') AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')
          ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

        // Nota Dinas Keluar
        public function notadinas_keluar($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*) as notadinas_keluar
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_From="'. $select_id . '")  AND (rr.ReceiverAs IN (  "to" ,"to_konsep","to_keluar",  "to_usul",  "to_reply", "to_forward"))
            AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a

            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

        // Nota Dinas Keluar ALl Unit
        public function notadinas_keluar_all_unit($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as notadinas_keluar_all_unit
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like '$select_id%')  AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar',  'to_usul',  'to_reply', 'to_forward'))
          AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')  ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

         // Nota Dinas Keluar ALl MK
         public function notadinas_keluar_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as notadinas_keluar_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like 'uk.1.%')  AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar',  'to_usul',  'to_reply', 'to_forward'))
          AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')  ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

        // Disposisi Keluar
        public function disposisi_keluar($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*) as disposisi_keluar
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_From="'. $select_id . '")  AND (rr.ReceiverAs = "cc1")AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")  ORDER BY ReceiveDate DESC) a


            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
        }

         // Disposisi Keluar All Unit
         public function disposisi_keluar_all_unit($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as disposisi_keluar_all_unit
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like '$select_id%' )  AND (rr.ReceiverAs = 'cc1')AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')  ORDER BY ReceiveDate DESC) a


          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

        // Disposisi Keluar
        public function disposisi_keluar_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as disposisi_keluar_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like 'uk.1.%' )  AND (rr.ReceiverAs = 'cc1')AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')  ORDER BY ReceiveDate DESC) a


          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }


         // Tembusan Keluar
         public function tembusan_keluar($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT COUNT(*) as tembusan_keluar
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
            AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
            AND (rr.RoleId_From="'. $select_id . '")  AND (rr.ReceiverAs = "bcc") AND (rr.ReceiveDate BETWEEN "'. $tgl1 . '" AND "'. $tgl2 . '")
            ORDER BY ReceiveDate DESC) a

            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();

            return $stmt;
          }

           // Tembusan Keluar
         public function tembusan_keluar_all_unit($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as tembusan_keluar_all_unit
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like '$select_id%')  AND (rr.ReceiverAs = 'bcc') AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')
          ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

         // Tembusan Keluar
         public function tembusan_keluar_all_mk($tgl1, $tgl2, $select_id) {

          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo($tgl1);die();
            // Query
          $query = "
          SELECT COUNT(*) as tembusan_keluar_all_mk
          FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
          Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
          i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey)
          AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) WHERE (i.NId IS NOT NULL)
          AND (rr.RoleId_From like 'uk.1.%')  AND (rr.ReceiverAs = 'bcc') AND (rr.ReceiveDate BETWEEN '$tgl1' and '$tgl2')
          ORDER BY ReceiveDate DESC) a

          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();

          return $stmt;
        }

         // User
         public function user($tgl1, $tgl2, $select_id) {

            // Check Request Methods
            $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
            checkRequestMethod($checkRequest);
            // echo($tgl1);die();
              // Query
            $query = '
            SELECT r.RoleId, r.RoleName, r.RoleDesc , mg.gjabatanName, p.PeopleName, p.PeopleId, p.PeoplePosition, p.RoleAtasan, p.no_nip FROM role r 
            JOIN master_gjabatan mg ON mg.gjabatanId = r.gjabatanId
            JOIN people p ON p.PrimaryRoleId = r.RoleId AND p.PeopleIsActive = 1 AND p.GroupId = 3
            WHERE r.RoleId="'. $select_id . '"

            ';

            //Prepare statement
            $stmt = $this->conn->prepare($query);

            //Execute query
            $stmt->execute();
            return $stmt;
        }

        public function statistik_unit($tgl1, $tgl2, $select_id){
          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);


          $query = "
          SELECT * FROM ( 
            SELECT CONCAT( 
              FLOOR(HOUR(SEC_TO_TIME(c.dt)) / 24), ' Hari ',MOD(HOUR(SEC_TO_TIME(c.dt)), 24), ' Jam ', MINUTE(SEC_TO_TIME(c.dt)), ' Menit ') AS Rate, g.PeopleName,c.jml AS jmlSurat , c.dt, c.RoleId_To  
              FROM ( SELECT b.RoleId_To,SUM(b.dt)/COUNT( b.RoleId_To) AS dt,COUNT( b.RoleId_To) AS jml 
              FROM ( SELECT a.PeopleName,a.PeoplePosition ,a.NId, a.RoleId_To, TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate) AS dt 
              FROM ( SELECT * FROM inbox_receiver d LEFT JOIN people e ON e. PrimaryRoleId = d.RoleId_To 
                WHERE d.readDate != '' AND d.ReceiveDate != '' AND d.RoleId_From != '' AND e.RoleAtasan LIKE '$select_id%'  AND (d.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:55:00') ) a ) b 
                GROUP BY b.RoleId_To ) c 
                RIGHT JOIN people g ON g.PrimaryRoleId = c.RoleId_To 
                WHERE g.PeopleIsActive = '1' AND c.jml != '' AND g.RoleAtasan LIKE '$select_id%' 
                UNION SELECT CONCAT( FLOOR(HOUR(SEC_TO_TIME(c.dt)) / 24), ' Hari ',MOD(HOUR(SEC_TO_TIME(c.dt)), 24), ' Jam ', MINUTE(SEC_TO_TIME(c.dt)), ' Menit ') AS Rate, g.PeopleName,c.jml AS jmlSurat ,c.dt,c.RoleId_To  
                FROM ( SELECT b.RoleId_To,SUM(b.dt)/COUNT( b.RoleId_To) AS dt,COUNT( b.RoleId_To) AS jml 
                FROM ( SELECT a.PeopleName,a.PeoplePosition ,a.NId, a.RoleId_To, TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate) AS dt 
                FROM ( SELECT * FROM inbox_receiver d LEFT JOIN people e ON e. PrimaryRoleId = d.RoleId_To 
                WHERE d.readDate != '' AND d.ReceiveDate != '' AND d.RoleId_From != '' AND e.PrimaryRoleId ='$select_id' AND (d.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:55:00') ) a ) b 
                GROUP BY b.RoleId_To ) c RIGHT JOIN people g ON g.PrimaryRoleId = c.RoleId_To LEFT JOIN Role h ON g.PrimaryRoleId = h.RoleId 
                WHERE g.PeopleIsActive = '1' AND c.jml != '' AND g.PrimaryRoleId= '$select_id' AND gjabatanId = 'XxJyPn38Yh.3' ) j 
                ORDER BY j.dt DESC 
          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }
        public function statistik_person($tgl1, $tgl2, $select_id,$select_roleid){
          // Check Request Methods
          $checkRequest = $_SERVER['REQUEST_METHOD'] === 'GET';
          checkRequestMethod($checkRequest);
          // echo $tgl1;
          // echo $tgl2;
          // echo$select_id;
          // echo $select_roleid;die();


          $query = "
          SELECT * FROM ( 
            SELECT CONCAT( 
              FLOOR(HOUR(SEC_TO_TIME(c.dt)) / 24), ' Hari ',MOD(HOUR(SEC_TO_TIME(c.dt)), 24), ' Jam ', MINUTE(SEC_TO_TIME(c.dt)), ' Menit ') AS Rate, g.PeopleName,c.jml AS jmlSurat , c.dt, c.RoleId_To  
              FROM ( SELECT b.RoleId_To,SUM(b.dt)/COUNT( b.RoleId_To) AS dt,COUNT( b.RoleId_To) AS jml 
              FROM ( SELECT a.PeopleName,a.PeoplePosition ,a.NId, a.RoleId_To, TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate) AS dt 
              FROM ( SELECT * FROM inbox_receiver d LEFT JOIN people e ON e. PrimaryRoleId = d.RoleId_To 
                WHERE d.readDate != '' AND d.ReceiveDate != '' AND d.RoleId_From != '' AND e.RoleAtasan LIKE '$select_id%'  AND (d.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:55:00') ) a ) b 
                GROUP BY b.RoleId_To ) c 
                RIGHT JOIN people g ON g.PrimaryRoleId = c.RoleId_To 
                WHERE g.PeopleIsActive = '1' AND c.jml != '' AND g.RoleAtasan LIKE '$select_id%' 
                UNION SELECT CONCAT( FLOOR(HOUR(SEC_TO_TIME(c.dt)) / 24), ' Hari ',MOD(HOUR(SEC_TO_TIME(c.dt)), 24), ' Jam ', MINUTE(SEC_TO_TIME(c.dt)), ' Menit ') AS Rate, g.PeopleName,c.jml AS jmlSurat ,c.dt,c.RoleId_To  
                FROM ( SELECT b.RoleId_To,SUM(b.dt)/COUNT( b.RoleId_To) AS dt,COUNT( b.RoleId_To) AS jml 
                FROM ( SELECT a.PeopleName,a.PeoplePosition ,a.NId, a.RoleId_To, TIMESTAMPDIFF(SECOND,a.ReceiveDate,a.readDate) AS dt 
                FROM ( SELECT * FROM inbox_receiver d LEFT JOIN people e ON e. PrimaryRoleId = d.RoleId_To 
                WHERE d.readDate != '' AND d.ReceiveDate != '' AND d.RoleId_From != '' AND e.PrimaryRoleId ='$select_id' AND (d.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:55:00') ) a ) b 
                GROUP BY b.RoleId_To ) c RIGHT JOIN people g ON g.PrimaryRoleId = c.RoleId_To LEFT JOIN Role h ON g.PrimaryRoleId = h.RoleId 
                WHERE g.PeopleIsActive = '1' AND c.jml != '' AND g.PrimaryRoleId= '$select_id' AND gjabatanId = 'XxJyPn38Yh.3' ) j 
                WHERE RoleId_To = '$select_roleid'
                ORDER BY j.dt DESC 
          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function list_unit(){
          $query = "
          SELECT r.RoleId , r.RoleDesc AS RoleDesc, p.PeopleName AS Pejabat,
          CASE r.RoleId
          WHEN 'uk.1.1' THEN 1
          WHEN 'uk.1.1.1' THEN 31
          WHEN 'uk.1.1.2' THEN 3
          WHEN 'uk.1.1.3' THEN 4
          WHEN 'uk.1.1.4' THEN 5
          WHEN 'uk.1.1.5' THEN 6
          WHEN 'uk.1.1.6' THEN 7
          WHEN 'uk.1.1.7' THEN 8
          WHEN 'uk.1.1.8' THEN 9
          WHEN 'uk.1.1.9' THEN 10
          WHEN 'uk.1.1.10' THEN 11
          WHEN 'uk.1.1.11' THEN 12
          WHEN 'uk.1.1.12' THEN 13
          WHEN 'uk.1.1.13' THEN 14
          WHEN 'uk.1.1.14' THEN 15
          WHEN 'uk.1.1.15' THEN 16
           WHEN 'uk.1.1.16' THEN 17
           WHEN 'uk.1.1.17' THEN 18
           WHEN 'uk.1.1.18' THEN 19
           WHEN 'uk.1.1.19' THEN 20
           WHEN 'uk.1.1.20' THEN 21
           WHEN 'uk.1.1.21' THEN 22
           WHEN 'uk.1.1.22' THEN 23
           WHEN 'uk.1.1.23' THEN 24
           WHEN 'uk.1.1.24' THEN 25
           WHEN 'uk.1.1.25' THEN 26
           WHEN 'uk.1.1.26' THEN 27
           WHEN 'uk.1.1.27' THEN 28
           WHEN 'uk.1.1.28' THEN 29
           WHEN 'uk.1.1.29' THEN 30
           WHEN 'uk.1.2.2' THEN 50
           WHEN 'uk.1.2.1' THEN 51
           WHEN 'uk.1.2.3' THEN 52
           WHEN 'uk.1.2' THEN 2
           
           
           END AS urutan 
         FROM role r  
         JOIN people p ON r.RoleId = p.PrimaryRoleId 
         WHERE (r.RoleParentId='uk.1.1' OR r.RoleId='uk.1.1' OR r.RoleParentId = 'uk.1.2' OR r.RoleId = 'uk.1.2')  AND (r.RoleStatus=1) AND p.GroupId=3  AND p.PeopleIsActive =1 AND (r.gjabatanId = 'XxJyPn38Yh.3' OR r.gjabatanId = 'XxJyPn38Yh.2')
         GROUP BY r.RoleId
         
         ORDER BY urutan ASC
         
         
         
         
         
          ";

          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function rapat_jmlhadir($select_id, $tgl1,$tgl2){
          $query = "
          SELECT COUNT(statusa) AS jmlhadir 
          FROM vrekaprapatwaktu 
          WHERE id_peserta ='$select_id' 
          AND statusa='hadir' 
          AND (DATE_FORMAT(START, '%Y-%m-%d') BETWEEN '$tgl1' AND '$tgl2')
           
          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }
        public function rapat_total($select_id, $tgl1,$tgl2){
          $query = "
          SELECT COUNT(id_peserta) AS total_rapat FROM vrekaprapatwaktu 
            WHERE id_peserta ='$select_id' 
            AND (DATE_FORMAT(START, '%Y-%m-%d') BETWEEN '$tgl1' AND '$tgl2')
           
          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function unread_ttd($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS unread_ttd
          FROM  (
              SELECT d.NId AS NId ,c.StatusReceive AS StatusReceive,c.Nomor AS Nomor,c.Instansipengirim AS InstansiPengirim,c.Hal AS Hal,Tgl AS Tgl,c.ReceiveDate AS ReceiveDate,
              c.Instansipengirim AS zz  
              FROM (SELECT cc.NId  FROM inbox_ds  cc 
                  JOIN (SELECT PeopleId FROM people WHERE PrimaryRoleId = '$select_id' AND GroupId = '3' LIMIT 1 ) bb ON cc.penanda_tangan = bb.PeopleId WHERE  cc.FileStatus ='draf_DS' ) d 
                  LEFT JOIN (SELECT a.NId AS NId,a.UrgensiId AS UrgensiId, a.Nomor AS Nomor,a.Hal AS Hal, a.Tgl AS Tgl,b.RoleId_To AS RoleId_To,b.StatusReceive AS StatusReceive,
                  a.Instansipengirim AS InstansiPengirim,b.ReceiveDate AS ReceiveDate,a.Instansipengirim AS zz 
                      FROM (SELECT NId, GIR_Id , RoleId_To , StatusReceive,ReceiveDate FROM inbox_receiver 
                      WHERE StatusReceive = 'unread'  
                      AND RoleId_To =  '$select_id' 
                      AND ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59'
                      GROUP BY NId DESC LIMIT 400) b 
                          JOIN inbox a ON a.NId = b.NId ) c ON d.NId = c.NId 
                          JOIN master_urgensi e ON e.UrgensiId = c.UrgensiId LIMIT 500) ee
          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function unread_notadinas($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS unread_notadinas
            FROM   (SELECT rr.ReceiverAs AS ReceiverAs, i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
              Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
              i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
                FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
                AND (rr.NId = i.NId)))) 
                  JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
                    WHERE (i.NId IS NOT NULL) 
                    AND (rr.RoleId_To=  '$select_id') 
                    AND (rr.StatusReceive = 'unread') 
                    AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar',  'to_usul',  'to_reply', 'to_forward')) 
                    AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                    ORDER BY ReceiveDate DESC) a

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function read_notadinas($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS read_notadinas
            FROM   (SELECT rr.ReceiverAs AS ReceiverAs, i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
              Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
              i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
                FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
                AND (rr.NId = i.NId)))) 
                  JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
                    WHERE (i.NId IS NOT NULL) 
                    AND (rr.RoleId_To=  '$select_id') 
                    AND (rr.ReceiverAs IN (  'to' ,'to_konsep','to_keluar',  'to_usul',  'to_reply', 'to_forward')) 
                    AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                    ORDER BY ReceiveDate DESC) a

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function unread_disposisi($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS unread_disposisi
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
              FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
              AND (rr.NId = i.NId)))) 
                JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
                  WHERE (i.NId IS NOT NULL) 
                  AND (rr.RoleId_To=  '$select_id') 
                  AND (rr.StatusReceive = 'unread') 
                  AND (rr.ReceiverAs = 'cc1') 
                  AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                  ORDER BY ReceiveDate DESC) a
      

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function read_disposisi($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS read_disposisi
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
              FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
              AND (rr.NId = i.NId)))) 
                JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
                  WHERE (i.NId IS NOT NULL) 
                  AND (rr.RoleId_To=  '$select_id') 
                  AND (rr.ReceiverAs = 'cc1') 
                  AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
                  ORDER BY ReceiveDate DESC) a
      

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function unread_tembusan($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS unread_tembusan
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
              FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
              AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
              WHERE (i.NId IS NOT NULL) 
              AND (rr.RoleId_To=  '$select_id') 
              AND (rr.StatusReceive = 'unread') 
              AND (rr.ReceiverAs = 'bcc')
              AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
              ORDER BY ReceiveDate DESC) a

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

        public function read_tembusan($select_id, $tgl1, $tgl2){
          $query = "
          SELECT COUNT(*) AS read_tembusan
            FROM   (SELECT  i.NId AS NId,i.Nomor AS Nomor,i.Hal AS Hal,i.NTipe AS NTipe,i.BerkasId AS BerkasId,mu.UrgensiName AS UrgensiName,i.
            Tgl AS Tgl,i.NIsi AS NIsi,i.CreatedBy AS CreatedBy,i.CreationRoleId AS CreationRoleId,rr.RoleId_To AS RoleId_To,rr.StatusReceive AS StatusReceive,
            i.Instansipengirim AS InstansiPengirim,rr.ReceiveDate AS ReceiveDate,i.Instansipengirim AS zz 
              FROM ((inbox i JOIN inbox_receiver rr ON(((rr.NKey = i.NKey) 
              AND (rr.NId = i.NId)))) JOIN master_urgensi mu ON((mu.UrgensiId = i.UrgensiId))) 
              WHERE (i.NId IS NOT NULL) 
              AND (rr.RoleId_To=  '$select_id') 
              AND (rr.ReceiverAs = 'bcc')
              AND (rr.ReceiveDate BETWEEN '$tgl1 00:00:00' AND '$tgl2 23:59:59')
              ORDER BY ReceiveDate DESC) a

          ";
          //Prepare statement
          $stmt = $this->conn->prepare($query);

          //Execute query
          $stmt->execute();
          return $stmt;
        }

    }



?>