<?php 
    Class Database {
        // DB Params
        private $host       = '192.168.30.55';
        private $db_name    = 'db_sikd_mk';
        private $username   = 'programmer';
        private $password   = 'Pr09r@m3r_m1512!';
        private $conn;


        //DB Connect
        public function connect(){
            $this->conn = null;

            try {
                $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name,
                $this->username , $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
                // This is for checking connectifity with DB
                // echo json_encode([
                //     'status' => 'success',
                //     'message' => 'Server SIKD is Healthy'
                // ]);
            } catch (PDOException $e){
                echo json_encode([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]);
                die();
            }
            return $this->conn;
        }
    }

?>