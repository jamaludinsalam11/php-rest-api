<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    
    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);
    $mailinbox = new ModMailInbox($db);
    // Get Query Params
    $select_id  = isset($_GET['select_id']) ? $_GET['select_id'] : '';
    $unitkerja = $select_id;
   
    // $result = $post->list_unit();
    $result = $mailinbox->listPegawai($unitkerja);
    // Get Row count
    $num = $result->rowCount();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $post_item = array(
                'RoleId'            => $RoleId,
                'RoleAtasan'        => $RoleAtasan,
                'RoleName'          => $RoleName,
                'RoleDesc'          => $RoleDesc,
                'PeopleName'        => $PeopleName,
                'pesan_masuk'       => []

            );
            $pesan_masuk = $post->pesan_masuk($RoleId);
            $total = $pesan_masuk->rowCount();
            // echo $total;die();
                
            // $row_receiver = $tmp->fetchAll();
            while($row_receiver = $pesan_masuk->fetch(PDO::FETCH_ASSOC)){
                // $total_pegawai = $total_pegawai + 1;
                array_push($post_item['pesan_masuk'], $row_receiver);

            }
            // Push to "data"
            array_push($posts_arr['data'], $post_item);

        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModStatistik',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModStatistik',
            'data'  => 'Not Found'
        ]);
    }

?>