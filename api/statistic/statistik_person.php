<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);

    // Get Query Params
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';
    $select_id  = isset($_GET['select_id']) ? $_GET['select_id'] : '';
    $select_roleid  = isset($_GET['select_roleid']) ? $_GET['select_roleid'] : '';
    // echo ($select_id);
    // echo ('echo tes');
    // die();
    // Query
    $result = $post->statistik_person($tgl1, $tgl2,$select_id, $select_roleid);
    // Get Row count
    $num = $result->rowCount();


   


    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();
        

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $post_item = array(
                'Rate'          => $Rate,
                'PeopleName'    => $PeopleName,
                'jmlSurat'      => $jmlSurat,
                'dt'            => $dt,
                'RoleId_To'     => $RoleId_To,
                'RoleAtasan'    => $select_

            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
            // array_push($posts_arr['total_surat_unit'], $total_surat)
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModStatistikUnit',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModStatistikUnit',
            'data'  => 'Not Found'
        ]);
    }

?>