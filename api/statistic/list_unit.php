<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);
    $mailinbox = new ModMailInbox($db);

    // Get Query Params
   
    $result = $post->list_unit();
    // Get Row count
    $num = $result->rowCount();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
           
            $post_item = array(
                'RoleId'        => $RoleId,
                'RoleDesc'      => $RoleDesc,
                'Pejabat'       => $Pejabat,
                'urutan'        => $urutan,
                'list_pegawai'  => []

            );
            $unitkerja = $RoleId;
            $list_pegawai = $mailinbox->listPegawai($unitkerja);
            $total = $list_pegawai->rowCount();
            
                
            // $row_receiver = $tmp->fetchAll();
            while($row_receiver = $list_pegawai->fetch(PDO::FETCH_ASSOC)){
                $total_pegawai = $total_pegawai + 1;
                array_push($post_item['list_pegawai'], $row_receiver);

            }


            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModStatistik',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModStatistik',
            'data'  => 'Not Found'
        ]);
    }

?>