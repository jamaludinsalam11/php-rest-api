<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);

    // Get Query Params
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';
    $select_id_pre  = isset($_GET['select_id']) ? $_GET['select_id'] : '';
    // echo ($select_id);
    // echo ('echo tes');
    // die();
    // Query

    if($select_id_pre == 'uk.1.1' || $select_id_pre == 'uk.1.2' ){
        $select_id = $select_id_pre;
        
    } else {
        $select_id = substr($select_id_pre, 0, 9);
        
    }

    $result = $post->disposisi_keluar_all_mk($tgl1, $tgl2, $select_id);
    // Get Row count
    $num = $result->rowCount();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $post_item = array(
                'disposisi_keluar_all_mk' => $disposisi_keluar_all_mk
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>