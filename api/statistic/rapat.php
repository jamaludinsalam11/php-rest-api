<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);

    // Get Query Params
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';
    $select_id  = isset($_GET['select_id']) ? $_GET['select_id'] : '';
    // echo ($select_id);
    // echo ('echo tes');
    // die();

    try{
        $tmpHadir = $post->rapat_jmlhadir($select_id, $tgl1, $tgl2);
        while($row_hadir = $tmpHadir->fetch(PDO::FETCH_ASSOC)){
            extract($row_hadir);
    
        }
    
        $tmpTotal = $post->rapat_total($select_id, $tgl1, $tgl2);
        while($row_total = $tmpTotal->fetch(PDO::FETCH_ASSOC)){
            extract($row_total);
    
        }

        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModRapat',
            'data'  => array ([
                'total_rapat'   => ceil($total_rapat),
                'jmlhadir'      => ceil($jmlhadir),
                'tidakhadir'    => ceil($total_rapat-$jmlhadir),
                'percentage'    => ceil(($jmlhadir / $total_rapat) * 100)
            ])
        ]);

    }catch (PDOException $e){
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModRapat',
            'message'  => $e->errorInfo
        ]);
    }

    

?>