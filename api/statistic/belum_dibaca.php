<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Statistic/Statistic.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Statistic($db);

    // Get Query Params
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';
    $select_id  = isset($_GET['select_id']) ? $_GET['select_id'] : '';
    // echo ($select_id);
    // echo ('echo tes');
    // die();
    // Query
    $result = $post->belum_dibaca($tgl1, $tgl2, $select_id);
    // Get Row count
    $num = $result->rowCount();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();
       
        $disposisi  = 0;
        $notadinas  = 0;
        $tembusan   = 0;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            if($ReceiverAs ==='Disposisi'){
                $disposisi = $disposisi + 1;
            } elseif($ReceiverAs === 'Nota Dinas'){
                $notadinas = $notadinas + 1;
            } else {
                $tembusan = $tembusan + 1;
            }

            $post_item = array(
                'ReceiverAs' => $ReceiverAs
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'    => 'success',
            'mod'       => 'ModMailTL',
            'total'     => $num,
            'disposisi' => $disposisi,
            'notadinas' => $notadinas,
            'tembusan'  => $tembusan,
            'data'      => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Failed',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>