<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Graph/Graph.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Graph($db);

    // Get Query Params
    $NId = isset($_GET['NId']) ? $_GET['NId'] : '';
    $GIR_Id = isset($_GET['GIR_Id']) ? $_GET['GIR_Id'] : '';
    // echo $NId; die();
    // Query
    $result = $post->getDisposisi($NId, $GIR_Id);
    // Get Row count
    // echo $result;die();
    $num = $result->rowCount();

    $result->status = 'OK';
    
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $post_item = array(
                'Disposisi'       => $Disposisi
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL - Get Disposisi ',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'Error',
            'mod' => 'ModMailTL - Get Disposisi',
            'data'  => 'Not Found'
        ]);
    }

?>