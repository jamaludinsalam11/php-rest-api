<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/Graph/Graph.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new Graph($db);

    // Get Query Params
    $NId = isset($_GET['NId']) ? $_GET['NId'] : '224.080819121019';
    
    // Query
    $result = $post->read($NId);
    // Get Row count
    $num = $result->rowCount();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $post_item = array(
                'NId'               => $NId,
                'GIR_Id'            => $GIR_Id,
                'From_Id'           => $From_Id,
                'pengirim'          => $pengirim,
                'penerima'          => $penerima,
                'jabatan_pengirim'  => $jabatan_pengirim,
                'jabatan_penerima'  => $jabatan_penerima,
                'To_Id'             => $To_Id,
                'RoleId_From'       => $RoleId_From,
                'NTglReg'           => $NTglReg,
                'ReceiveDate'       => $ReceiveDate,
                'ReceiverAs'        => $ReceiverAs,
                'readDate'          => $readDate,
                'JenisName'         => $JenisName,
                'JabatanPengirim'   => $JabatanPengirim,
                'JabatanPenerima'   => $JabatanPenerima,
                'pesan_disposisi'   => $pesan_disposisi,
                'Msg'               => $Msg,
                'Rate'              => $Rate,
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>
