<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    // Get Query Params
    $NId        = isset($_GET['NId']) ? $_GET['NId'] : '224.080819121019';
    $begin      = isset($_GET['begin']) ? $_GET['begin'] : '224.080819121019';
    $row        = isset($_GET['row']) ? $_GET['row'] : '224.080819121019';
    $primaryroleid = isset($_GET['primaryroleid']) ? $_GET['primaryroleid'] : '224.080819121019';
    $sumber     = isset($_GET['sumber']) ? $_GET['sumber'] : '224.080819121019';
    $read       = isset($_GET['read']) ? $_GET['read'] : '224.080819121019';
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '224.080819121019';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '224.080819121019';
    $unit       = isset($_GET['unit']) ? $_GET['unit'] : '224.080819121019';
    $sort       = isset($_GET['sort']) ? $_GET['sort'] : '224.080819121019';
    $q          = isset($_GET['q']) ? $_GET['q'] : '224.080819121019';
    $search     = isset($_GET['search']) ? $_GET['search'] : '224.080819121019';
    // echo $sumber;die();
    // Query
    // $result2 = $post->readCount($begin,$row, $primaryroleid, $sumber, $read, $tgl1,$tgl2, $unit, $sort, $search, $q);
    // $num2 = $result2->rowCount();
    // echo $num2;die();
    $result = $post->read($begin,$row, $primaryroleid, $sumber, $read, $tgl1,$tgl2, $unit, $sort, $search, $q);
    // Get Row count
    // $result_count = $post->read_count($begin,$row, $primaryroleid, $sumber, $read, $tgl1,$tgl2, $unit, $sort, $search, $q);
    $num = $result->rowCount();
    // $num2 = $result_count->rowCount();
    // echo $result_count->rowCount();die();
    $query_count = "SELECT FOUND_ROWS()";
    $num2 = $db->query($query_count)->fetchColumn();


    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();
        $total_data = 0;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $total_data = $total_data + 1;
            $post_item = array(
                'To_Id'             => $To_Id,
                'RoleId_To'         => $RoleId_To,
                'NId'               => $NId,
                'StatusReceive'     => $StatusReceive,
                'Nomor'             => $Nomor,
                'InstansiPengirim'  => $InstansiPengirim,
                'Hal'               => $Hal,
                'Tgl'               => $Tgl,
                'ReceiveDate'       => $ReceiveDate,
                'zz'                => $zz,
                'ds'                => $ds,
                'Pengirim'          => $Pengirim,
                'JabatanPengirim'   => $JabatanPengirim
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'        => 'success',
            'mod'           => 'ModMailTL',
            'current_page'  => $begin + 0,
            'prev_page'     => $begin - 1,
            'next_page'     => $begin + 1,
            'row_per_page'  => $_GET['row'] + 0,
            'total_pages'   => ceil($num2/($_GET['row'] + 0)),
            'total_data'    => $num2,
            'data'          => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>