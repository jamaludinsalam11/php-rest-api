<?php 

    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

   

    // Get Query Params
    // $tgl1        = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    // $tgl2        = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';

    // echo $tgl1;die();
    $tgl1 = $_GET['tgl1'];
    $tgl2 = $_GET['tgl2'];
    $RoleId = $_GET['RoleId'];
    // echo $RoleId;die();
    // Query
    $result = $post->list_pegawai_unitkerja($RoleId);
    // Get Row count
    $num = $result->rowCount();
    // echo $num;die();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();
        $counter = 1;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            
            // Response Time
            // $tmpResponseTime = $post->respon_pegawai_unitkerja($PrimaryRoleId);
            // while($row_response_time = $tmpResponseTime->fetch(PDO::FETCH_ASSOC)){
            //     extract($row_response_time);

            // }
            // Surat Masuk
            // $tmpSuratMasuk = $post->surat_masuk_pegawai_unitkerja($PrimaryRoleId);
            // while($row_surat_masuk = $tmpSuratMasuk->fetch(PDO::FETCH_ASSOC)){
            //     extract($row_surat_masuk);

            // }
            
            $post_item = array(
                'id'                => $counter++,
                'PeopleName'        => $PeopleName,
                'PeoplePosition'    => $PeoplePosition,
                'PrimaryRoleId'     => $PrimaryRoleId,
                'RoleAtasan'        => $RoleAtasan,
                'PeopleId'          => $PeopleId,
                'response_time'     => $waktu_response,
                'wt2'               => null,
                'surat_masuk'       => null,
                'surat_keluar'       => null
               

            );



            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'        => 'success',
            'mod'           => 'ModMailTL',
            'total_data'    => $num,
            'data'          => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>