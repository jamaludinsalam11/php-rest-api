<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    // header("Access-Control-Allow-Credentials" : true );


    header('Content-Type: application/json');
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    $data = json_decode(file_get_contents("php://input"));
    $nid            = $data->nid;
    $peopleid       = $data->peopleid;
    $primaryroleid  = $data->primaryroleid;
    $penting_id     = $nid . '-' . $primaryroleid;

    // echo $penting_id; die();
    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    

    try{
        $post->delete_penting($penting_id);
        echo json_encode([
            'status'        => 'success',
            'mod'           => 'ModDeletePenting',
            'message'       => "Berhasil Menghapus penting"
        ]);
    } catch (PDOException $e){
        echo json_encode([
            'status'        => 'failed',
            'mod'           => 'ModDeletePenting',
            'message'       => $e->errorInfo
        ]);
    }
    
 
   
   

?>