<?php 

    //Headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    $data = json_decode(file_get_contents("php://input"));

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    $NId            = $data->NId;
    $GIR_Id         = $data->GIR_Id;
    $To_Id          = $data->To_Id;
    $RoleId_To      = $data->RoleId_To;
    $readDate       = $data->readDate;


    
    $result = $post->update_toread($NId, $GIR_Id, $To_Id, $RoleId_To, $readDate);
    // Get Row count
    // $num = $result->rowCount();
    // echo $num;die();

    // $result->status = 'OK';
    // Check if any data
    if(!empty($NId) && !empty($GIR_Id) && !empty($To_Id) && !empty($RoleId_To) && !empty($readDate)){
        try{
            $post->update_toread($NId, $GIR_Id, $To_Id, $RoleId_To, $readDate);
            echo json_encode([
                'status'        => 'success',
                'mod'           => 'ModUpdateRead',
                'message'       => "Berhasil Mengubah unread to read"
            ]);
        } catch (PDOException $e){
            echo json_encode([
                'status'        => 'failed',
                'mod'           => 'ModUpdateRead',
                'message'       => $e->errorInfo
            ]);
        }
    } else {
        echo json_encode([
            'status'        => 'failed',
            'mod'           => 'ModUpdateRead',
            'message'       => 'Gagal mengubah unread to read , periksa body upload, pastikan tidak kosong'
        ]);
    }
    // if($result){
    //     echo json_encode([
    //         'status'        => 'success',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => "Berhasil Mengubah unread to read"
    //     ]);
    // } else {
    //     echo json_encode([
    //         'status'        => 'failed',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => 'Gagal mengubah unread to read'
    //     ]);
    // }

    // if($post->update_toread($NId, $GIR_Id, $To_Id, $RoleId_To, $readDate){
    //     echo json_encode([
    //         'status'        => 'success',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => "Berhasil Mengubah unread to read"
    //     ]);
    // } else {
    //     echo json_encode([
    //         'status'        => 'failed',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => 'Gagal mengubah unread to read'
    //     ]);
    // }

    // if()
    // try{
    //     $post->update_toread($NId, $GIR_Id, $To_Id, $RoleId_To, $readDate);
    //     echo json_encode([
    //         'status'        => 'success',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => "Berhasil Mengubah unread to read"
    //     ]);
    // } catch (PDOException $e){
    //     echo json_encode([
    //         'status'        => 'failed',
    //         'mod'           => 'ModUpdateRead',
    //         'message'       => $e->errorInfo
    //     ]);
    // }

?>