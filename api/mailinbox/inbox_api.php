<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    // Get Query Params
    $nid        = isset($_GET['nid']) ? $_GET['nid'] : '224.080819121019';
    $begin      = isset($_GET['begin']) ? $_GET['begin'] : '224.080819121019';
    $row        = isset($_GET['row']) ? $_GET['row'] : '224.080819121019';
    $primaryroleid = isset($_GET['primaryroleid']) ? $_GET['primaryroleid'] : '224.080819121019';
    $sumber     = isset($_GET['sumber']) ? $_GET['sumber'] : '224.080819121019';
    $read       = isset($_GET['read']) ? $_GET['read'] : '224.080819121019';
    $tgl1       = isset($_GET['tgl1']) ? $_GET['tgl1'] : '224.080819121019';
    $tgl2       = isset($_GET['tgl2']) ? $_GET['tgl2'] : '224.080819121019';
    $unit       = isset($_GET['unit']) ? $_GET['unit'] : '224.080819121019';
    $sort       = isset($_GET['sort']) ? $_GET['sort'] : '224.080819121019';
    $q          = isset($_GET['q']) ? $_GET['q'] : '224.080819121019';
    $search     = isset($_GET['search']) ? $_GET['search'] : '224.080819121019';
    // echo $sumber;die();
    // Query
    $result = $post->inbox_api($nid, $begin,$row, $primaryroleid, $sumber, $read, $tgl1,$tgl2, $unit, $sort, $search, $q);
    
    // Get Row count
    $num = $result->rowCount();
    // echo $num;die();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            
            $post_item = array(
                'NId'               => $NId,
                'StatusReceive'     => $StatusReceive,
                'Nomor'             => $Nomor,
                'InstansiPengirim'  => $InstansiPengirim,
                'Hal'               => $Hal,
                'Tgl'               => $Tgl,
                'ReceiveDate'       => $ReceiveDate,
                'zz'                => $zz,
                'ds'                => $ds,
                'Pengirim'          => $Pengirim,
                'JabatanPengirim'   => $JabatanPengirim,
                'inbox_receiver'    => []
            );
            // foreach($post_item as $index=>$item){
                $tmp = $post->inbox_receiver($NId);
                
                // $row_receiver = $tmp->fetchAll();
                while($row_receiver = $tmp->fetch(PDO::FETCH_ASSOC)){
                    array_push($post_item['inbox_receiver'], $row_receiver);
                }
                // $row_receiver = $tmp->fetch(PDO::FETCH_ASSOC);
                // array_push($post_item['inbox_receiver'], $row_receiver);
                // array_push($post_item[$index]['inbox_receiver'], $tmp);
                // $post_item['inbox_receiver'] = $row_receiver;
            // }

            // Push to "data"
            array_push($posts_arr['data'], $post_item);

        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 1.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'        => 'success',
            'mod'           => 'ModMailTL',
            'total_data'    => $num,
            'data'          => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 1.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>