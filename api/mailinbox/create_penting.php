<?php 


    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    $data = json_decode(file_get_contents("php://input"));
    $nid            = $data->nid;
    $peopleid       = $data->peopleid;
    $primaryroleid  = $data->primaryroleid;
    $date           = $data->date;
    $hal            = $data->hal;   
    $instansipengirim = $data->instansipengirim;   
    $jabatanpengirim  = $data->jabatanpengirim;   
    $nomor            = $data->nomor;   
    $pengirim         = $data->pengirim;   
    $receivedate      = $data->receivedate;   
    $statusreceive    = $data->statusreceive;   
    $tgl            = $data->tgl;   
    $zz             = $data->zz;   
    $ds            = $data->ds;   

    // echo json_decode([
    //     'hal' => $hal,
    //     'instansipengirim' => $instansipengirim,
    //     'jabatanpengirim' => $jabatanpengirim,
    //     'nomor' => $nomor,
    //     'pengirim' => $pengirim,
    //     'receivedate' => $receivedate,
    //     'statusreceive' => $statusreceive,
    //     'tgl' => $tgl,
    //     'zz' => $zz,
    //     'ds' => $ds,
    // ]); die();
    // echo $hal;die();


    $penting_id     = $nid . '-' . $primaryroleid;

    // echo $penting_id; die();
    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    

    try{
        $post->create_penting($nid ,$peopleid ,$primaryroleid, $date, $penting_id, $hal, $instansipengirim, $jabatanpengirim,$nomor, $pengirim, $receivedate, $statusreceive, $tgl, $zz, $ds );
        echo json_encode([
            'status'        => 'success',
            'mod'           => 'ModCreatePenting',
            'message'          => "Berhasil Menandai penting"
        ]);
    } catch (PDOException $e){
        echo json_encode([
            'status'        => 'failed',
            'mod'           => 'ModCreatePenting',
            'message'          => $e->errorInfo
        ]);
    }
    
 
   
   

?>