<?php 

    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

   

    // Get Query Params
    // $tgl1        = isset($_GET['tgl1']) ? $_GET['tgl1'] : '';
    // $tgl2        = isset($_GET['tgl2']) ? $_GET['tgl2'] : '';

    // echo $tgl1;die();
    $tgl1 = $_GET['tgl1'];
    $tgl2 = $_GET['tgl2'];
    // echo $tgl2;die();
    // Query
    $result = $post->list_unitkerja();
    // Get Row count
    $num = $result->rowCount();
    // echo $num;die();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            
           
            
            $post_item = array(
                'RoleId'            => $RoleId,
                'RoleName'          => $RoleName,      
                'RoleDesc'          => $RoleDesc,      
                'RoleParentId'      => $RoleParentId,      
                'gjabatanId'        => $gjabatanId,
                'RoleStatus'        => $RoleStatus,
                

            );



            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'        => 'success',
            'mod'           => 'ModMailTL',
            'total_data'    => $num,
            'data'          => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'success',
            'mod' => 'ModMailTL',
            'data'  => 'Not Found'
        ]);
    }

?>