<?php 

    //Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: GET');

    include_once '../../config/Database.php';
    include_once '../../Mod/ModMailInbox/ModMailInbox.php';

    // Instatiate DB $ Connect
    $database = new Database();
    $db = $database->connect();

    //Instantiate ModInboxReceiver
    $post = new ModMailInbox($db);

    // Get Query Params
    $nid                = isset($_GET['nid']) ? $_GET['nid'] : '224.080819121019';
    $penting_id         = isset($_GET['penting_id ']) ? $_GET['penting_id '] : '224.080819121019';
    $peopleid           = $_GET['peopleid'];
    $primaryroleid      = $_GET['primaryroleid'] ;
    // $NId        = isset($_GET['NId']) ? $_GET['NId'] : '224.080819121019';
    // echo $peopleid;die();
    // Query
    $result = $post->get_penting($peopleid , $primaryroleid);
    // Get Row count
    $num = $result->rowCount();
    // echo $num;die();

    $result->status = 'OK';
    // Check if any data
    if($num > 0){
        $posts_arr = array();
        $posts_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            
            $post_item = array(
                'penting_id'        => $penting_id,
                'NId'               => $NId,
                'PeopleId'          => $PeopleId,      
                'PrimaryRoleId'     => $PrimaryRoleId,     
                'created_date'      => $created_date,
                'Hal'               => $Hal,
                'InstansiPengirim'  => $InstansiPengirim,
                'JabatanPengirim'   => $JabatanPengirim,
                'Nomor'             => $Nomor,
                'Pengirim'          => $Pengirim,
                'ReceiveDate'       => $ReceiveDate,
                'StatusReceive'     => $StatusReceive,
                'Tgl'               => $Tgl,
                'zz'                => $zz,
                'ds'                => $ds,
                
            );

            // Push to "data"
            array_push($posts_arr['data'], $post_item);
        }
        
        // Turn to JSON Output
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status'        => 'success',
            'mod'           => 'ModGetPenting',
            'data'          => $posts_arr['data']
        ]);
    } else {
        // No Posts
        echo json_encode([
            'api' => [
                'version' => 2.0,
                'engine' => "PHP Native ::".phpversion()."",
                'database' => mysqli_get_client_info(),  
            ],
            'status' => 'failed',
            'mod' => 'ModGetPenting',
            'data'  => 'Not Found'
        ]);
    }

?>